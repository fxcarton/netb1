#include "netb1.h"

#ifndef NETB1_FORUM_H
#define NETB1_FORUM_H

/* Data types */

struct Thread {
    char* url;
    char* title;
    char* author;
    unsigned long creationDate;
    unsigned long lastModificationDate;
};

#define THREAD_DEFAULT_FORMAT "%u - %t\n"

void thread_fprintf(const struct Thread* thread, const char* format, FILE* out);
void thread_free(struct Thread* thread);

struct Post {
    char* url;
    char* title;
    char* author;
    unsigned long creationDate;
    unsigned long lastModificationDate;
    char* content;
};

#define POST_DEFAULT_FORMAT "%u - %t\n"

void post_fprintf(const struct Post* post, const char* format, FILE* out);
void post_free(struct Post* post);

/* Plugins */

struct ForumPlugin {
    int (*search)(const char* query, struct Thread** results, unsigned int* nbResults, const void* pluginArgs);
};

/* Actions */

enum ForumActions {
    FORUM_SEARCH,
    FORUM_NUM_ACTIONS
};

extern const struct ForumAction {
    const char* name;
    int (*action)(const struct ForumPlugin*, int argc, char** argv, const void* pluginArgs);
    void (*usage)(FILE*);
} forumActions[FORUM_NUM_ACTIONS];

#define FORUM_MAIN(plugin, argc, argv, pluginArgs) NETB1_MAIN(forum, plugin, argc, argv, pluginArgs)
#define FORUM_USAGE(prog, file) NETB1_USAGE(forum, prog, file)

#endif
