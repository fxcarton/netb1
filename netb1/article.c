#include <stdlib.h>
#include "article.h"

#define check(s) ((s) ? (s) : "(none)")

void article_fprintf(const struct Article* article, const char* format, FILE* out) {
    for (; *format; format++) {
        if (*format == '%') {
            switch (*(++format)) {
                case 'u': fputs(check(article->url), out); break;
                case 't': fputs(check(article->title), out); break;
                case 'a': fputs(check(article->author), out); break;
                case 's': fputs(check(article->snippet), out); break;
                case 'c': fputs(check(article->content), out); break;
                case 0:   return;
                default:  fputc(*format, out); break;
            }
        } else {
            fputc(*format, out);
        }
    }
}

void article_free(struct Article* article) {
    free(article->url);
    free(article->title);
    free(article->author);
    free(article->snippet);
    free(article->content);
}

static void article_search_usage(FILE* file) {
    fprintf(file,
            "search [--format-string=<format>] <query>\n"
            "    list the search results\n"
            "    --format-string: custom format string\n"
            "        %%u: URL, %%t: title, %%a: author\n"
            "        %%s: snippet, %%c: content\n"
           );
}

static int article_search(const struct ArticlePlugin* plugin, int argc, char** argv, const void* pluginArgs) {
    NETB1_SEARCH(Article, article, ARTICLE)
}

static void article_view_usage(FILE* file) {
    fprintf(file,
            "view [--lang=id] <title>\n"
            "    list the search results\n"
           );
}

static int article_view(const struct ArticlePlugin* plugin, int argc, char** argv, const void* pluginArgs) {
    struct Article result;

    if (argc == 0) {
        fputs("Usage:\n", stdout);
        article_view_usage(stdout);
        return 1;
    } else if (argc != 1) {
        fputs("Usage:\n", stderr);
        article_view_usage(stderr);
    } else if (!plugin->get_article(argv[0], &result, pluginArgs)) {
        fprintf(stderr, "Failed to send the request or parse the answer\n");
    } else {
        article_fprintf(&result, "%t\n\n%c\n", stdout);
        article_free(&result);
        return 1;
    }

    return 0;
}

const struct ArticleAction articleActions[ARTICLE_NUM_ACTIONS] = {
    {"search", article_search, article_search_usage},
    {"view", article_view, article_view_usage}
};
