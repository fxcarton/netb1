#include "netb1.h"

#ifndef NETB1_VIDEO_H
#define NETB1_VIDEO_H

/* Data types */

struct Video {
    char* url;
    char* title;
    char* description;
    char* author;
    unsigned long length;
    unsigned long views;
    unsigned long publishedDate;
    char* thumbnailUrl;
};

#define VIDEO_DEFAULT_FORMAT "%u (%H:%M:%S) %t\n"

void video_fprintf(const struct Video* video, const char* format, FILE* out);
void video_free(struct Video* video);

struct VideoStream {
    char* url;
    char* format;
    char* description;
};

void video_stream_fprintf(const struct VideoStream* videoStream, const char* format, FILE* out);
void video_stream_free(struct VideoStream* videoStream);

/* Plugins */

struct VideoPlugin {
    int (*search)(const char* query, struct Video** results, unsigned int* nbResults, const void* pluginArgs);
    int (*get_video)(const char* id, struct Video* result, const void* pluginArgs);
    int (*get_related_videos)(const char* id, unsigned int* nbRelatedVideos, struct Video** relatedVideos, const void* pluginArgs);
};

/* Actions */

enum VideoActions {
    VIDEO_SEARCH,
    VIDEO_INFO,
    VIDEO_RELATED,
    VIDEO_NUM_ACTIONS
};

extern const struct VideoAction {
    const char* name;
    int (*action)(const struct VideoPlugin*, int argc, char** argv, const void* pluginArgs);
    void (*usage)(FILE*);
} videoActions[VIDEO_NUM_ACTIONS];

#define VIDEO_MAIN(plugin, argc, argv, pluginArgs) NETB1_MAIN(video, plugin, argc, argv, pluginArgs)
#define VIDEO_USAGE(prog, file) NETB1_USAGE(video, prog, file)

#endif
