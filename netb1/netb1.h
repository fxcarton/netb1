#include <stdio.h>
#include <string.h>

#ifndef NETB1_H
#define NETB1_H

#define NETB1_MAIN(type, plugin, argc, argv, pluginArgs) { \
    unsigned int _i; \
    if ((argc) >= 1) { \
        for (_i = 0; _i < sizeof(type##Actions) / sizeof(type##Actions[0]); _i++) { \
            if (!strcmp((argv)[0], type##Actions[_i].name) && type##Actions[_i].action) { \
                return !type##Actions[_i].action((plugin), (argc) - 1, (argv) + 1, pluginArgs); \
            } \
        } \
    } \
}

#define NETB1_USAGE(type, prog, file) { \
    unsigned int _i; \
    fprintf((file), "Usage: %s [pluginargs] <action> [args]\nActions:\n", (prog)); \
    for (_i = 0; _i <  sizeof(type##Actions) / sizeof(type##Actions[0]); _i++) { \
        if (type##Actions[_i].action) { \
            type##Actions[_i].usage(file); \
        } \
    } \
}

#define NETB1_SEARCH(Type, type, TYPE) { \
    struct Type* results; \
    unsigned int j, nbResults; \
    const char* format = TYPE##_DEFAULT_FORMAT; \
    int i; \
 \
    for (i = 0; i < argc; i++) { \
        if (!strncmp(argv[i], "--format-string=", 16)) { \
            format = argv[i] + 16; \
        } else { \
            break; \
        } \
    } \
 \
    if (i == argc) { \
        fputs("Usage:\n", stdout); \
        type##_search_usage(stdout); \
        return 1; \
    } else if (i != argc - 1) { \
        fputs("Usage:\n", stderr); \
        type##_search_usage(stderr); \
    } else if (!plugin->search(argv[i], &results, &nbResults, pluginArgs)) { \
        fprintf(stderr, "Error: failed to send the request or parse the answer\n"); \
    } else { \
        for (j = 0; j < nbResults; j++) { \
            type##_fprintf(results + j, format, stdout); \
            type##_free(results + j); \
        } \
        free(results); \
        return 1; \
    } \
    return 0; \
}

#endif
