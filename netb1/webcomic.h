#include "netb1.h"

#ifndef NETB1_WEBCOMIC_H
#define NETB1_WEBCOMIC_H

/* Data types */

struct Webcomic {
    char* title;
    char* description;
    char* imgUrl;
    char* url;
    char* prevUrl;
    char* nextUrl;
};

#define WEBCOMIC_DEFAULT_FORMAT "Title: %t\nDescription: %d\nURL: %u\nImage: %i\n"

void webcomic_fprintf(const struct Webcomic* webcomic, const char* format, FILE* out);
void webcomic_free(struct Webcomic* webcomic);

/* Plugins */

struct WebcomicPlugin {
    int (*get)(const char* url, struct Webcomic* result, const void* pluginArgs);
    int (*get_first)(struct Webcomic* result, const void* pluginArgs);
    int (*get_last)(struct Webcomic* result, const void* pluginArgs);
};

/* Actions */

enum WebcomicActions {
    WEBCOMIC_INFO,
    WEBCOMIC_GET_FIRST,
    WEBCOMIC_GET_LAST,
    WEBCOMIC_NUM_ACTIONS
};

extern const struct WebcomicAction {
    const char* name;
    int (*action)(const struct WebcomicPlugin*, int argc, char** argv, const void* pluginArgs);
    void (*usage)(FILE*);
} webcomicActions[WEBCOMIC_NUM_ACTIONS];

#define WEBCOMIC_MAIN(plugin, argc, argv, pluginArgs) NETB1_MAIN(webcomic, plugin, argc, argv, pluginArgs)
#define WEBCOMIC_USAGE(prog, file) NETB1_USAGE(webcomic, prog, file)

#endif
