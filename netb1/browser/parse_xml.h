#ifndef PARSE_XML_H
#define PARSE_XML_H

#include <libxml/tree.h>
#include <libxml/HTMLparser.h>


xmlNodePtr deep_search(xmlNodePtr root, char* name, char* propName, char* propValue);

void print_node_props(xmlNodePtr ptr);
void print_children(const xmlNodePtr ptr);

#endif
