#include <curl/curl.h>

#ifndef NETB1_BROWSER_DOWNLOAD_H
#define NETB1_BROWSER_DOWNLOAD_H

struct PtrSize {
    char* ptr;
    size_t size;
};

size_t write_to_str(void* buffer, size_t size, size_t nmemb, void* ptrsize);
size_t write_to_file(void* buffer, size_t size, size_t nmemb, void* file);

struct curl_slist* make_headers(const char* first, ...);
int set_user_agent(CURL* curl);

int dl_page_payload(const char* url, struct PtrSize* result);

#endif
