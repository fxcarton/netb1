#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include "download.h"

size_t write_to_str(void* buffer, size_t size, size_t nmemb, void* userptr) {
    struct PtrSize* ptrsize = userptr;
    size_t total = ptrsize->size + (size *= nmemb);
    char* tmp;
    if ((tmp = realloc(ptrsize->ptr, total))) {
        memcpy(tmp + ptrsize->size, buffer, size);
        ptrsize->ptr = tmp;
        ptrsize->size = total;
        return size;
    }
    return 0;
}

size_t write_to_file(void* buffer, size_t size, size_t nmemb, void* userptr) {
    return fwrite(buffer, 1, size * nmemb, userptr);
}

struct curl_slist* make_headers(const char* first, ...) {
    struct curl_slist *ret = NULL, *tmp;
    const char* current = first;
    va_list ap;
    va_start(ap, first);
    while (current) {
        tmp = curl_slist_append(ret, current);
        if (!tmp) {
            curl_slist_free_all(ret);
            va_end(ap);
            return NULL;
        }
        ret = tmp;
        current = va_arg(ap, const char*);
    }
    va_end(ap);
    return ret;
}

int set_user_agent(CURL* curl) {
    return curl_easy_setopt(curl, CURLOPT_USERAGENT, "Netb1/0.1") == CURLE_OK;
}

int dl_page_payload(const char* url, struct PtrSize* result) {
    CURL* curl;
    int ok = 0;

    if (!(curl = curl_easy_init())) {
        return 0;
    }

    result->ptr = NULL;
    result->size = 0;
    if ( curl_easy_setopt(curl, CURLOPT_URL, url) == CURLE_OK
      && curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_to_str) == CURLE_OK
      && curl_easy_setopt(curl, CURLOPT_WRITEDATA, result) == CURLE_OK
      && curl_easy_perform(curl) == CURLE_OK) {
        ok = 1;
    }
    curl_easy_cleanup(curl);

    if (ok) {
        return 1;
    }
    free(result->ptr);
    return 0;
}
