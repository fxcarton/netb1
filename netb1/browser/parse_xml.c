#include "parse_xml.h"

xmlNodePtr deep_search(xmlNodePtr root, char* name, char* propName, char* propValue) {
    xmlNodePtr ret = NULL;

    if (!root) {
        return NULL;
    }

    if (xmlStrcmp(root->name, (xmlChar*) name) == 0) {
        if (propName) {
            xmlChar* idProp = xmlGetProp(root, (xmlChar*) propName);
            if (xmlStrcmp(idProp, (xmlChar*) propValue) == 0 || !propValue) {
                ret = root;
            }
            xmlFree(idProp);
        } else {
            ret = root;
        }
    }

    if (!ret) {
        xmlNodePtr cur = root->children;

        while (cur && !ret) {
            ret = deep_search(cur, name, propName, propValue);
            cur = cur->next;
        }
    }

    return ret;
}

void print_node_props(xmlNodePtr ptr) {
    xmlAttrPtr attr;

    for (attr = ptr->properties; attr; attr = attr->next) {
        xmlChar* value = xmlGetProp(ptr, attr->name);
        printf("    %s=%s\n", attr->name, value);
        xmlFree(value);
    }
}

void print_children(const xmlNodePtr ptr) {
    xmlNodePtr cur = ptr->children;

    while (cur) {
        printf("%s\n", cur->name);
        print_node_props(cur);
        cur = cur->next;
    }
}

