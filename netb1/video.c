#include <stdlib.h>
#include "video.h"

#define check(s) ((s) ? (s) : "(none)")

void video_fprintf(const struct Video* video, const char* format, FILE* out) {
    unsigned long h = video->length / 3600;
    unsigned long m = (video->length - h * 3600) / 60;
    unsigned long s = video->length - h * 3600 - m * 60;

    for (; *format; format++) {
        if (*format == '%') {
            switch (*(++format)) {
                case 'u': fputs(check(video->url), out); break;
                case 't': fputs(check(video->title), out); break;
                case 'd': fputs(check(video->description), out); break;
                case 'a': fputs(check(video->author), out); break;
                case 'l': fprintf(out, "%lu", video->length); break;
                case 'H': fprintf(out, "%02lu", h); break;
                case 'M': fprintf(out, "%02lu", m); break;
                case 'S': fprintf(out, "%02lu", s); break;
                case 'v': fprintf(out, "%lu", video->views); break;
                case 'b': fputs(check(video->thumbnailUrl), out); break;
                case 0:   return;
                default:  fputc(*format, out); break;
            }
        } else {
            fputc(*format, out);
        }
    }
}

void video_free(struct Video* video) {
    free(video->url);
    free(video->title);
    free(video->description);
    free(video->author);
    free(video->thumbnailUrl);
}

void video_stream_fprintf(const struct VideoStream* videoStream, const char* format, FILE* out) {
    for (; *format; format++) {
        if (*format == '%') {
            switch (*(++format)) {
                case 'u': fputs(check(videoStream->url), out); break;
                case 'f': fputs(check(videoStream->format), out); break;
                case 'd': fputs(check(videoStream->description), out); break;
                case 0:   return;
                default:  fputc(*format, out); break;
            }
        } else {
            fputc(*format, out);
        }
    }
}

void video_stream_free(struct VideoStream* videoStream) {
    free(videoStream->url);
    free(videoStream->format);
    free(videoStream->description);
}

static void video_search_usage(FILE* file) {
    fprintf(file,
            "search [--format-string=<format>] <query>\n"
            "    list the search results\n"
            "    --format-string: custom format string\n"
            "        %%u: URL, %%t: title, %%d: description, %%a: author\n"
            "        %%l: length, %%v: views, %%b: thumbnail URL\n"
           );
}

static int video_search(const struct VideoPlugin* plugin, int argc, char** argv, const void* pluginArgs) {
    NETB1_SEARCH(Video, video, VIDEO)
}

static void video_info_usage(FILE* file) {
    fprintf(file,
            "info [--format-string=<string>] <videoId>\n"
            "    show information about a video\n"
            "    --format-string: custom format string\n"
            "        %%u: URL, %%t: title; %%d: description, %%a: author\n"
            "        %%l: length, %%v: views, %%b: thumbnail URL\n"
           );
}

static int video_info(const struct VideoPlugin* plugin, int argc, char** argv, const void* pluginArgs) {
    struct Video video;
    const char* format = VIDEO_DEFAULT_FORMAT;
    int i;

    for (i = 0; i < argc; i++) {
        if (!strncmp(argv[i], "--format-string=", 16)) {
            format = argv[i] + 16;
        } else {
            break;
        }
    }

    if (i == argc) {
        fputs("Usage:\n", stdout);
        video_info_usage(stdout);
        return 1;
    } else if (i != argc - 1) {
        fputs("Usage:\n", stderr);
        video_info_usage(stderr);
    } else if (!plugin->get_video(argv[i], &video, pluginArgs)) {
        fprintf(stderr, "Failed to retrieve video information\n");
    } else {
        video_fprintf(&video, format, stdout);
        video_free(&video);
        return 1;
    }
    return 0;
}

static void video_related_usage(FILE* file) {
    fprintf(file,
            "related [--format-string=<string>] <videoId>\n"
            "    list related videos\n"
            "    --format-string: custom format string\n"
            "        %%u: URL, %%t: title; %%d: description, %%a: author\n"
            "        %%l: length, %%v: views, %%b: thumbnail URL\n"
           );
}

static int video_related(const struct VideoPlugin* plugin, int argc, char** argv, const void* pluginArgs) {
    unsigned int nbRelatedVideos;
    struct Video* relatedVideos;
    const char* format = VIDEO_DEFAULT_FORMAT;
    int i;
    unsigned int j;

    for (i = 0; i < argc; i++) {
        if (!strncmp(argv[i], "--format-string=", 16)) {
            format = argv[i] + 16;
        } else {
            break;
        }
    }

    if (i == argc) {
        fputs("Usage:\n", stdout);
        video_related_usage(stdout);
        return 1;
    } else if (i != argc - 1) {
        fputs("Usage:\n", stderr);
        video_related_usage(stderr);
    } else if (!plugin->get_related_videos(argv[i], &nbRelatedVideos, &relatedVideos, pluginArgs)) {
        fprintf(stderr, "Failed to retrieve related videos.\n");
    } else {
        for (j = 0; j < nbRelatedVideos; j++) {
            video_fprintf(relatedVideos + j, format, stdout);
            video_free(relatedVideos + j);
        }
        free(relatedVideos);
        return 1;
    }
    return 0;
}

const struct VideoAction videoActions[VIDEO_NUM_ACTIONS] = {
    {"search", video_search, video_search_usage},
    {"info", video_info, video_info_usage},
    {"related", video_related, video_related_usage}
};
