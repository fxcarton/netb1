#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netb1/browser/download.h>
#include "webcomic.h"

#define check(s) ((s) ? (s) : "(none)")

void webcomic_fprintf(const struct Webcomic* webcomic, const char* format, FILE* out) {
    for (; *format; format++) {
        if (*format == '%') {
            switch (*(++format)) {
                case 't': fputs(check(webcomic->title), out); break;
                case 'd': fputs(check(webcomic->description), out); break;
                case 'i': fputs(check(webcomic->imgUrl), out); break;
                case 'u': fputs(check(webcomic->url), out); break;
                case 'p': fputs(check(webcomic->prevUrl), out); break;
                case 'n': fputs(check(webcomic->nextUrl), out); break;
                case 0:   return;
                default:  fputc(*format, out); break;
            }
        } else {
            fputc(*format, out);
        }
    }
}

void webcomic_free(struct Webcomic* webcomic) {
    free(webcomic->title);
    free(webcomic->description);
    free(webcomic->imgUrl);
    free(webcomic->url);
    free(webcomic->prevUrl);
    free(webcomic->nextUrl);
}

static void webcomic_get_usage(FILE* file, const char* func) {
    fprintf(file,
            "%s [--format-string=<string>] [--download[=file]]\n"
            "    show %s comic\n"
            "    --download: download the picture to file (defaults to stdout)\n",
            func, func
           );
}

static void webcomic_info_usage(FILE* file) {
    fprintf(file,
            "info [--format-string=<string>] [--download[=file]] <url/id>\n"
            "    show specified comic\n"
            "    --download: download the picture to file (defaults to stdout)\n"
           );
}

static int webcomic_do(const struct Webcomic* webcomic, const char* printFormat, FILE* download) {
    int ret = 1;

    webcomic_fprintf(webcomic, printFormat, stdout);

    if (download) {
        CURL* curl;
        ret = (!((curl = curl_easy_init())
           && curl_easy_setopt(curl, CURLOPT_URL, webcomic->imgUrl) == CURLE_OK
           && curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_to_file) == CURLE_OK
           && curl_easy_setopt(curl, CURLOPT_WRITEDATA, download) == CURLE_OK
           && curl_easy_perform(curl) == CURLE_OK)
           );
        if (ret) {
            fprintf(stderr, "Error: failed to download '%s'\n", webcomic->imgUrl);
        }
        if (curl) {
            curl_easy_cleanup(curl);
        }
    }

    return ret;
}

static int webcomic_get(int (*const func)(struct Webcomic*, const void*), void (*const usage)(FILE*), int argc, char** argv, const void* pluginArgs) {
    int i, ret = 0;
    const char* format = WEBCOMIC_DEFAULT_FORMAT;
    struct Webcomic webcomic;
    FILE* file = NULL;

    for (i = 0; i < argc; i++) {
        if (!strncmp(argv[i], "--format-string=", 16)) {
            format = argv[i] + 16;
        } else if (!strncmp(argv[i], "--download", 10)) {
            if (file) {
                fprintf(stderr, "Error: multiple --download is not allowed\n");
                return 0;
            }
            switch (argv[i][10]) {
                case '=':
                    if (!(file = fopen(argv[i] + 11, "wb"))) {
                        fprintf(stderr, "Error: failed to open '%s'\n", argv[i] + 11);
                        return 0;
                    }
                    continue;
                case 0:
                    file = stdout;
                    continue;
            }
            break;
        } else {
            break;
        }
    }
    if (i != argc) {
        fputs("Usage:\n", stderr);
        usage(stderr);
    } else if (!func(&webcomic, pluginArgs)) {
        fprintf(stderr, "Error: failed to retrieve webcomic information\n");
    } else {
        ret = webcomic_do(&webcomic, format, file);
        webcomic_free(&webcomic);
    }

    if (file && file != stdout) {
        fclose(file);
    }
    return ret;
}

static int webcomic_info(const struct WebcomicPlugin* plugin, int argc, char** argv, const void* pluginArgs) {
    int i, ret = 0;
    const char* format = WEBCOMIC_DEFAULT_FORMAT;
    struct Webcomic webcomic;
    FILE* file = NULL;

    for (i = 0; i < argc; i++) {
        if (!strncmp(argv[i], "--format-string=", 16)) {
            format = argv[i] + 16;
        } else if (!strncmp(argv[i], "--download", 10)) {
            if (file) {
                fprintf(stderr, "Error: multiple --download is not allowed\n");
                return 0;
            }
            switch (argv[i][10]) {
                case '=':
                    if (!(file = fopen(argv[i] + 11, "wb"))) {
                        fprintf(stderr, "Error: failed to open '%s'\n", argv[i] + 11);
                        return 0;
                    }
                    continue;
                case 0:
                    file = stdout;
                    continue;
            }
            break;
        } else {
            break;
        }
    }
    if (i != argc - 1) {
        fputs("Usage:\n", stderr);
        webcomic_info_usage(stderr);
    } else if (!plugin->get(argv[i], &webcomic, pluginArgs)) {
        fprintf(stderr, "Error: failed to retrieve webcomic information\n");
    } else {
        ret = webcomic_do(&webcomic, format, file);
        webcomic_free(&webcomic);
    }

    if (file && file != stdout) {
        fclose(file);
    }
    return ret;
}

static void webcomic_last_usage(FILE* file) {
    webcomic_get_usage(file, "last");
}

static int webcomic_last(const struct WebcomicPlugin* plugin, int argc, char** argv, const void* pluginArgs) {
    return webcomic_get(plugin->get_last, webcomic_last_usage, argc, argv, pluginArgs);
}

static void webcomic_first_usage(FILE* file) {
    webcomic_get_usage(file, "first");
}

static int webcomic_first(const struct WebcomicPlugin* plugin, int argc, char** argv, const void* pluginArgs) {
    return webcomic_get(plugin->get_first, webcomic_first_usage, argc, argv, pluginArgs);
}

const struct WebcomicAction webcomicActions[WEBCOMIC_NUM_ACTIONS] = {
    {"info", webcomic_info, webcomic_info_usage},
    {"first", webcomic_first, webcomic_first_usage},
    {"last", webcomic_last, webcomic_last_usage},
};
