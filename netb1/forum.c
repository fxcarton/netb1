#include <stdlib.h>
#include "forum.h"

#define check(s) ((s) ? (s) : "(none)")

void thread_fprintf(const struct Thread* thread, const char* format, FILE* out) {
    for (; *format; format++) {
        if (*format == '%') {
            switch (*(++format)) {
                case 'u': fputs(check(thread->url), out); break;
                case 't': fputs(check(thread->title), out); break;
                case 'a': fputs(check(thread->author), out); break;
                case 0:   return;
                default:  fputc(*format, out); break;
            }
        } else {
            fputc(*format, out);
        }
    }
}

void thread_free(struct Thread* thread) {
    free(thread->url);
    free(thread->title);
    free(thread->author);
}

static void thread_search_usage(FILE* file) {
    fprintf(file,
            "search <query>\n"
            "    list the search results\n"
           );
}

static int thread_search(const struct ForumPlugin* plugin, int argc, char** argv, const void* pluginArgs) {
    NETB1_SEARCH(Thread, thread, THREAD)
}

void post_fprintf(const struct Post* post, const char* format, FILE* out) {
    for (; *format; format++) {
        if (*format == '%') {
            switch (*(++format)) {
                case 'u': fputs(check(post->url), out); break;
                case 't': fputs(check(post->title), out); break;
                case 'a': fputs(check(post->author), out); break;
                case 0:   return;
                default:  fputc(*format, out); break;
            }
        } else {
            fputc(*format, out);
        }
    }
}

void post_free(struct Post* post) {
    free(post->url);
    free(post->title);
    free(post->author);
}

const struct ForumAction forumActions[FORUM_NUM_ACTIONS] = {
    {"search", thread_search, thread_search_usage}
};
