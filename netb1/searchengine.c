#include <stdlib.h>

#include "searchengine.h"

#define check(s) ((s) ? (s) : "(none)")

void search_result_fprintf(const struct SearchResult* res, const char* format, FILE* out) {
    for (; *format; format++) {
        if (*format == '%') {
            switch (*(++format)) {
                case 'u': fputs(check(res->url), out); break;
                case 't': fputs(check(res->title), out); break;
                case 'd': fputs(check(res->description), out); break;
                case 0:   return;
                default:  fputc(*format, out); break;
            }
        } else {
            fputc(*format, out);
        }
    }
}

void search_result_free(struct SearchResult* res) {
    free(res->url);
    free(res->title);
    free(res->description);
}

static void search_result_search_usage(FILE* file) {
    fprintf(file,
            "search [--format-string=<string>] <query>\n"
            "    list the search results\n"
           "     --format-string: custom format string\n"
           "         %%u: URL, %%t: title; %%d: description\n"
           );
}

static int search_result_search(const struct SearchEnginePlugin* plugin, int argc, char** argv, const void* pluginArgs) {
    NETB1_SEARCH(SearchResult, search_result, SEARCH_RESULT)
}

const struct SearchEngineAction searchEngineActions[SEARCH_ENGINE_NUM_ACTIONS] = {
    {"search", search_result_search, search_result_search_usage}
};
