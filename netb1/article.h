#include "netb1.h"

#ifndef NETB1_ARTICLE_H
#define NETB1_ARTICLE_H

/* Data types */

struct Article {
    char* url;
    char* title;
    char* author;
    char* snippet;
    char* content;
    unsigned long date;
};

#define ARTICLE_DEFAULT_FORMAT "%u %t\n"

void article_fprintf(const struct Article* article, const char* format, FILE* out);
void article_free(struct Article* article);

/* Plugins */

struct ArticlePlugin {
    int (*search)(const char* query, struct Article** results, unsigned int* nbResults, const void* pluginArgs);
    int (*get_article)(const char* id, struct Article* result, const void* pluginArgs);
};

/* Actions */

enum ArticleActions {
    ARTICLE_SEARCH,
    ARTICLE_GET,
    ARTICLE_NUM_ACTIONS
};

extern const struct ArticleAction {
    const char* name;
    int (*action)(const struct ArticlePlugin*, int argc, char** argv, const void* pluginArgs);
    void (*usage)(FILE*);
} articleActions[ARTICLE_NUM_ACTIONS];

#define ARTICLE_MAIN(plugin, argc, argv, pluginArgs) NETB1_MAIN(article, plugin, argc, argv, pluginArgs)
#define ARTICLE_USAGE(prog, file) NETB1_USAGE(article, prog, file)

#endif
