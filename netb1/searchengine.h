#include "netb1.h"

#ifndef NETB1_SEARCHENGINE_H
#define NETB1_SEARCHENGINE_H

/* Data types */

struct SearchResult {
    char* url;
    char* title;
    char* description;
};

#define SEARCH_RESULT_DEFAULT_FORMAT "%u %t\n"

void search_result_fprintf(const struct SearchResult* result, const char* format, FILE* out);
void search_result_free(struct SearchResult* result);

/* Plugins */

struct SearchEnginePlugin {
    int (*search)(const char* query, struct SearchResult** results, unsigned int* nbResults, const void* pluginArgs);
};

/* Actions */

enum SearchEngineActions {
    SEARCH_ENGINE_SEARCH,
    SEARCH_ENGINE_NUM_ACTIONS
};

extern const struct SearchEngineAction {
    const char* name;
    int (*action)(const struct SearchEnginePlugin*, int argc, char** argv, const void* pluginArgs);
    void (*usage)(FILE*);
} searchEngineActions[SEARCH_ENGINE_NUM_ACTIONS];

#define SEARCH_ENGINE_MAIN(plugin, argc, argv, pluginArgs) NETB1_MAIN(searchEngine, plugin, argc, argv, pluginArgs)
#define SEARCH_ENGINE_USAGE(prog, file) NETB1_USAGE(searchEngine, prog, file)

#endif
