#include <plugins/searchengine/startpage.h>

int main(int argc, char** argv) {
    SEARCH_ENGINE_MAIN(startpage, argc - 1, argv + 1, NULL);

    SEARCH_ENGINE_USAGE(argv[0], stderr);
    return 1;
}
