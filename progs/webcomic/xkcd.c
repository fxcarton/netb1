#include <plugins/webcomic/xkcd.h>

int main(int argc, char** argv) {
    WEBCOMIC_MAIN(xkcd, argc - 1, argv + 1, NULL);

    WEBCOMIC_USAGE(argv[0], stderr);
    return 1;
}
