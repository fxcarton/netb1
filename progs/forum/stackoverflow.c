#include <plugins/forum/stackoverflow.h>

int main(int argc, char** argv) {
    FORUM_MAIN(stackoverflow, argc - 1, argv + 1, NULL);

    FORUM_USAGE(argv[0], stderr);
    return 1;
}
