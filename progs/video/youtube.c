#include <plugins/video/youtube.h>

int main(int argc, char** argv) {
    VIDEO_MAIN(youtube, argc - 1, argv + 1, NULL);

    VIDEO_USAGE(argv[0], stderr);
    return 1;
}
