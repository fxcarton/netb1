#include <string.h>
#include <plugins/article/wikipedia.h>

int main(int argc, char** argv) {
    const char* lang = "en";
    int i;

    for (i = 1; i < argc; i++) {
        if (!strncmp(argv[i], "--lang=", 7)) {
            lang = argv[i] + 7;
        } else {
            break;
        }
    }

    ARTICLE_MAIN(wikipedia, argc - i, argv + i, lang);

    ARTICLE_USAGE(argv[0], stderr);
    fprintf(stderr, "Plugin args:\n   --lang=id: wikipedia language code\n");
    return 1;
}
