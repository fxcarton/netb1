#include <netb1/searchengine.h>

#ifndef NETB1_SEARCHENGINE_STARTPAGE_H
#define NETB1_SEARCHENGINE_STARTPAGE_H

int startpage_search(const char* query, struct SearchResult** results, unsigned int* nbResults);

extern const struct SearchEnginePlugin* startpage;

#endif
