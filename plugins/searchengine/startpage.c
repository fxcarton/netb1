#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include <netb1/browser/download.h>
#include <netb1/browser/parse_xml.h>

#include "startpage.h"

static char* sanitize_string(const char* str) {
    char* ret;

    if ((ret = malloc((strlen(str)+1)*sizeof(char)))) {
        int i;

        for (i = 0; i < strlen(str); i++) {
            if (isalpha(str[i]) || isdigit(str[i])) {
                ret[i] = str[i];
            } else {
                ret[i] = '+';
            }
        }
        ret[i] = '\0';
    } else {
        return NULL;
    }

    return ret;
}

static char* get_search_url(const char* query) {
    static const char baseUrl[] = "https://www.startpage.com/do/dsearch?query=";
    char* searchUrl;
    char* sanitizedQuery = sanitize_string(query);

    if (sanitizedQuery && (searchUrl = malloc((sizeof(baseUrl) + strlen(sanitizedQuery))))) {
        strcpy(searchUrl, baseUrl);
        strcpy(searchUrl + sizeof(baseUrl) - 1, sanitizedQuery);
    } else {
        free(sanitizedQuery);
        return NULL;
    }
    free(sanitizedQuery);
    return searchUrl;
}

static int parse_result(xmlNodePtr resultPtr, struct SearchResult** results, unsigned int* nbResults, int resIdx) {
    char propName[128];
    xmlNodePtr link, title;
    struct SearchResult* tmp;

    if ((tmp = realloc(*results, (++(*nbResults))*sizeof(struct SearchResult)))) {
        tmp[*nbResults-1].url = NULL;
        tmp[*nbResults-1].title = NULL;
        tmp[*nbResults-1].description = NULL;
        sprintf(propName, "title_%d", resIdx);
        link = deep_search(resultPtr, "a", "id", propName);

        if (link) {
            xmlChar* linkUrl = xmlGetProp(link, (xmlChar*) "href");

            if (((tmp[*nbResults - 1]).url = malloc(strlen((char*)linkUrl)+1))) {
                strcpy(tmp[*nbResults - 1].url, (char*) linkUrl);
            } else {
                (*nbResults)--;
                xmlFree(linkUrl);
                return 0;
            }
            xmlFree(linkUrl);
        }

        title = deep_search(resultPtr, "span", "class", "result_url_heading");

        if (title) {
            xmlNodePtr titlePtr = deep_search(title, "text", NULL, NULL);

            if (titlePtr && ((tmp[*nbResults - 1]).title = malloc(strlen((char*)titlePtr->content)+1))) {
                strcpy(tmp[*nbResults - 1].title, (char*) titlePtr->content);
            } else {
                (*nbResults)--;
                return 0;
            }
        }

        *results = tmp;
    }

    return 1;
}

static int parse_page_payload(const struct PtrSize* ptr, struct SearchResult** results, unsigned int* nbResults) {
    htmlDocPtr doc = htmlReadMemory(ptr->ptr, ptr->size, "noname.xml", NULL, HTML_PARSE_NOBLANKS | HTML_PARSE_NOERROR);
    xmlNodePtr cur;
    xmlNodePtr curResult = NULL;
    int resIdx = 1;

    *nbResults = 0;
    *results = NULL;

    if (doc == NULL) {
        fprintf(stderr, "startpage: error parsing payload (broken HTML)\n");
        return 0;
    }

    cur = xmlDocGetRootElement(doc);

    cur = deep_search(cur, "ol", "class", "web_regular_results");

    do {
        char propName[128];

        sprintf(propName, "result%d", resIdx);
        curResult = deep_search(cur, "li", "id", propName);
        if (curResult) {
            if (!parse_result(curResult, results, nbResults, resIdx)) {
                fprintf(stderr, "startpage: warning, parsing a search result failed\n");
            }
        }

        resIdx++;
    } while (curResult);

    xmlFreeDoc(doc);
    xmlCleanupParser();

    return 1;
}

int startpage_search(const char* query, struct SearchResult** results, unsigned int* nbResults) {
    struct PtrSize reply = {NULL, 0};
    char* url;
    int ret = (url = get_search_url(query)) && dl_page_payload(url, &reply) && parse_page_payload(&reply, results, nbResults);

    free(reply.ptr);
    free(url);
    return ret;
}

static int _startpage_search(const char* query, struct SearchResult** threads, unsigned int* nbSearchResults, const void* pluginArgs) {
    return startpage_search(query, threads, nbSearchResults);
}

static const struct SearchEnginePlugin _startpage = {
    _startpage_search
};
const struct SearchEnginePlugin* startpage = &_startpage;
