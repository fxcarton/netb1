#include <netb1/video.h>

#ifndef NETB1_VIDEO_YOUTUBE_H
#define NETB1_VIDEO_YOUTUBE_H

const char* youtube_get_video_id(const char* url);

int youtube_get_video(const char* string /* id or url */, struct Video* video);
int youtube_get_related_videos(const char* string /* id or url */, unsigned int* nbRelatedVideos, struct Video** relatedVideos);
int youtube_get_video_streams(const char* string /* id or url */, unsigned int* nbStreams, struct VideoStream** videoStreams);

int youtube_search(const char* query, struct Video** results, unsigned int* nbResults);

extern const struct VideoPlugin* youtube;

#endif
