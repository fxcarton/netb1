#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <jansson.h>
#include <netb1/browser/download.h>
#include "youtube.h"

#define make_youtube_headers() make_headers("X-YouTube-Client-Name: 1", "X-YouTube-Client-Version: 2.20190626", NULL)

const char* youtube_get_video_id(const char* url) {
    static const char s1[] = "watch?v=";
    static const char s2[] = "&v=";
    const char* id;
    if ((id = strstr(url, s1))) {
        id += sizeof(s1) - 1;
    } else if ((id = strstr(url, s2))) {
        id += sizeof(s2) - 1;
    }
    return id;
}

static char* get_text(json_t* v, const char* name) {
    const char* orig;
    char* ret = NULL;
    if ((v = json_object_get(v, name)) && (orig = json_string_value(v)) && (ret = malloc(strlen(orig) + 1))) {
        strcpy(ret, orig);
    }
    return ret;
}

static char* get_simple_text(json_t* v, const char* name) {
    return ((v = json_object_get(v, name))) ? get_text(v, "simpleText") : NULL;
}

static char* get_runs_text(json_t* v, const char* name) {
    return ((v = json_object_get(v, name)) && (v = json_object_get(v, "runs")) && (v = json_array_get(v, 0))) ? get_text(v, "text") : NULL;
}

static unsigned long views_str_to_int(const char* views) {
    unsigned long c = 0;
    do {
        c = strtoul(views, (char**)&views, 10) + 1000 * c;
        for (; *views && (*views == ',' || (unsigned)(*views) >= 0x80); views++);
    } while (isdigit(*views));
    return c;
}

static unsigned long length_str_to_int(const char* length) {
    unsigned l = 0;
    do {
        l = l * 60 + strtoul(length, (char**)&length, 10);
    } while (*length++ == ':');
    return l;
}

static void parse_video(json_t* json, struct Video* video) {
    json_t* p;
    const char* s;
    char* tmp;
    if ((p = json_object_get(json, "videoId")) && (s = json_string_value(p)) && (video->url = malloc(34 + strlen(s)))) {
        sprintf(video->url, "https://www.youtube.com/watch?v=%s", s);
    } else {
        video->url = NULL;
    }
    if (!(video->title = get_simple_text(json, "title"))) {
        video->title = get_runs_text(json, "title");
    }
    video->description = get_runs_text(json, "descriptionSnippet");
    video->author = get_runs_text(json, "longBylineText");
    video->length = ((tmp = get_simple_text(json, "lengthText"))) ? length_str_to_int(tmp) : 0; free(tmp);
    video->views = ((tmp = get_simple_text(json, "viewCountText"))) ? views_str_to_int(tmp) : 0; free(tmp);
    video->publishedDate = /* TODO: ((s = get_simple_text(json, "publishedTimeText"))) */ 0;
    if ((p = json_object_get(json, "thumbnail")) && (p = json_object_get(p, "thumbnails")) && (p = json_array_get(p, 0)) && (p = json_object_get(p, "url"))
     && (s = json_string_value(p)) && (video->thumbnailUrl = malloc(strlen(s) + 1))) {
        strcpy(video->thumbnailUrl, s);
    } else {
        video->thumbnailUrl = NULL;
    }
}

static char* get_info_url(const char* id) {
    static const char url[] = "https://www.youtube.com/watch?pbj=1&v=";
    char *result, *ptr;

    if ((result = malloc(sizeof(url) + strlen(id)))) {
        strcpy(result, url);
        strcpy(result + sizeof(url) - 1, id);
        if ((ptr = strchr(result + sizeof(url) - 1, '&'))) {
            *ptr = 0;
        }
    }

    return result;
}

static int get_video_info(const char* id, struct PtrSize* result) {
    CURL* curl;
    struct curl_slist* headers = NULL;
    char* url = NULL;
    int ok;

    if (!(curl = curl_easy_init())) {
        return 0;
    }
    result->ptr = NULL;
    result->size = 0;
    ok = (headers = make_youtube_headers())
      && (url = get_info_url(id))
      && curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers) == CURLE_OK
      && curl_easy_setopt(curl, CURLOPT_URL, url) == CURLE_OK
      && curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_to_str) == CURLE_OK
      && curl_easy_setopt(curl, CURLOPT_WRITEDATA, result) == CURLE_OK
      && curl_easy_perform(curl) == CURLE_OK;
    free(url);
    curl_slist_free_all(headers);
    curl_easy_cleanup(curl);
    return ok;
}

int youtube_get_video(const char* string, struct Video* video) {
    static const char baseUrl[] = "https://www.youtube.com/watch?v=";
    struct PtrSize result = {NULL, 0};
    json_t *root, *e;
    char* tmp;
    const char* id;
    int ok;

    if (!(id = youtube_get_video_id(string))) {
        id = string;
    }

    if (!get_video_info(id, &result) ||!(root = json_loadb(result.ptr, result.size, 0, NULL))) {
        free(result.ptr);
        return 0;
    }

    ok = ((e = json_array_get(root, 2)) && (e = json_object_get(e, "playerResponse")) && (e = json_object_get(e, "videoDetails")));
    if (ok) {
        if ((video->url = malloc(sizeof(baseUrl) + strlen(id)))) {
            strcpy(video->url, baseUrl);
            strcpy(video->url + sizeof(baseUrl) - 1, id);
        }
        video->title = get_text(e, "title");
        video->description = get_text(e, "shortDescription");
        video->author = get_text(e, "author");
        video->length = (tmp = get_text(e, "lengthSeconds")) ? strtoul(tmp, NULL, 10) : 0; free(tmp);
        video->views = (tmp = get_text(e, "viewCount")) ? strtoul(tmp, NULL, 10) : 0; free(tmp);
        video->publishedDate = 0;
        video->thumbnailUrl = ((e = json_object_get(e, "thumbnail")) && (e = json_object_get(e, "thumbnails")) && (e = json_array_get(e, 0))) ? get_text(e, "url") : NULL;
    }

    json_decref(root);
    free(result.ptr);
    return ok;
}

static int _youtube_get_video(const char* string, struct Video* video, const void* pluginArgs) {
    return youtube_get_video(string, video);
}

int youtube_get_related_videos(const char* string, unsigned int* nbRelatedVideos, struct Video** relatedVideos) {
    struct PtrSize result = {NULL, 0};
    json_t *root, *e, *p, *q;
    unsigned int i, n;
    const char* id;
    int ok = 0;

    if (!(id = youtube_get_video_id(string))) {
        id = string;
    }

    if (!get_video_info(id, &result) ||!(root = json_loadb(result.ptr, result.size, 0, NULL))) {
        free(result.ptr);
        return 0;
    }

    ok = ((e = json_array_get(root, 3)) && (e = json_object_get(e, "response")) && (e = json_object_get(e, "contents")) && (e = json_object_get(e, "twoColumnWatchNextResults"))
       && (e = json_object_get(e, "secondaryResults")) && (e = json_object_get(e, "secondaryResults")) && (e = json_object_get(e, "results")));
    if (ok) {
        n = json_array_size(e);
        *nbRelatedVideos = 0;
        if ((*relatedVideos = malloc(n * sizeof(struct Video)))) {
            for (i = 0; i < n; i++) {
                if ((p = json_array_get(e, i))) {
                    if ((q = json_object_get(p, "compactAutoplayRenderer"))) {
                        p = (q = json_object_get(q, "contents")) ? json_array_get(q, 0) : NULL;
                    }
                    if ((p = json_object_get(p, "compactVideoRenderer"))) {
                        parse_video(p, (*relatedVideos) + (*nbRelatedVideos));
                        (*nbRelatedVideos)++;
                    }
                }
            }
        }
        ok = (*relatedVideos) != NULL;
    }

    json_decref(root);
    free(result.ptr);
    return ok;
}

static int _youtube_get_related_videos(const char* string, unsigned int* nbRelatedVideos, struct Video** relatedVideos, const void* pluginArgs) {
    return youtube_get_related_videos(string, nbRelatedVideos, relatedVideos);
}

static unsigned int hex(char c) {
    if ('0' <= c && c <= '9') {
        return c - '0';
    } else if ('A' <= c && c <= 'F') {
        return c - 'A' + 0xA;
    } else if ('a' <= c && c <= 'f') {
        return c - 'a' + 0xA;
    }
    return -1;
}

static char* unescape(const char* s, unsigned int n) {
    char *result, *dest;
    unsigned int a, b;
    if ((dest = result = malloc(n + 1))) {
        while (n) {
            if (*s == '%') {
                a = hex(s[1]); b = hex(s[2]);
                if (a == -1 || b == -1) {
                    free(result);
                    return NULL;
                }
                *dest++ = (a << 4) | b;
                s += 3;
                n -= 3;
            } else {
                *dest++ = *s++;
                n--;
            }
        }
        *dest = 0;
    }
    return result;
}

int youtube_get_video_streams(const char* string, unsigned int* nbStreams, struct VideoStream** videoStreams) {
    struct PtrSize result = {NULL, 0};
    json_t *root, *e;
    const char *formats, *ptr, *end, *endKey;
    char* tmp;
    const char* id;
    unsigned int i, iType, iQuality, iUrl, n;
    int ok = 0;

    if (!(id = youtube_get_video_id(string))) {
        id = string;
    }

    if (!get_video_info(id, &result) ||!(root = json_loadb(result.ptr, result.size, 0, NULL))) {
        free(result.ptr);
        return 0;
    }

    ok = ((e = json_array_get(root, 2)) && (e = json_object_get(e, "player")) && (e = json_object_get(e, "args"))
       && (e = json_object_get(e, "url_encoded_fmt_stream_map")) && (formats = json_string_value(e)));
    if (ok) {
        ok = iType = iQuality = iUrl = n = 0;
        for (ptr = formats; *ptr; ptr++) {
            n += (*ptr == '&' || *ptr == ',');
        }
        n = (n + 1) / 4;
        if ((*videoStreams = malloc(n * sizeof(struct VideoStream)))) {
            for (ptr = formats; *ptr; ptr = end) {
                for (end = ptr; *end && *end != '&' && *end != ','; end++);
                if (!(endKey = strchr(ptr, '=')) || endKey > end) {
                    break;
                }
                endKey++;
                if (!strncmp(ptr, "type=", 5)) {
                    if (iType >= n) break;
                    (*videoStreams)[iType++].format = unescape(endKey, end - endKey);
                } else if (!strncmp(ptr, "quality=", 8)) {
                    if (iQuality >= n) break;
                    (*videoStreams)[iQuality].description = unescape(endKey, end - endKey);
                    if ((tmp = strchr((*videoStreams)[iQuality].description, ','))) {
                        *tmp = 0;
                    }
                    iQuality++;
                } else if (!strncmp(ptr, "url=", 4)) {
                    if (iUrl >= n) break;
                    (*videoStreams)[iUrl++].url = unescape(endKey, end - endKey);
                }
                if (*end) {
                    end++;
                }
            }
            *nbStreams = (iType < iQuality) ? ((iQuality < iUrl) ? iUrl : iQuality) : ((iType < iUrl) ? iUrl : iType);
            for (i = iType; i < *nbStreams; i++) {
                (*videoStreams)[i].format = NULL;
            }
            for (i = iQuality; i < *nbStreams; i++) {
                (*videoStreams)[i].description = NULL;
            }
            for (i = iUrl; i < *nbStreams; i++) {
                (*videoStreams)[i].url = NULL;
            }
            ok = !(*ptr);
        }
    }

    json_decref(root);
    free(result.ptr);
    return ok;
}

static char* get_search_url(const char* query) {
    static const char url[] = "https://www.youtube.com/results?pbj=1&search_query=";
    char *result, *ptr;
    size_t n = sizeof(url) + strlen(query);
    if (!(result = malloc(n))) {
        return NULL;
    }
    strcpy(result, url);
    strcpy(result + sizeof(url) - 1, query);
    for (ptr = result; *ptr; ptr++) {
        if (*ptr == ' ') {
            *ptr = '+';
        }
    }
    return result;
}

static int search(const char* query, struct PtrSize* result) {
    CURL* curl;
    struct curl_slist* headers = NULL;
    char* url = NULL;
    int ok = 0;

    if (!(curl = curl_easy_init())) {
        return 0;
    }
    result->ptr = NULL;
    result->size = 0;
    if ((headers = make_youtube_headers())
     && (url = get_search_url(query))
     && curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers) == CURLE_OK
     && curl_easy_setopt(curl, CURLOPT_URL, url) == CURLE_OK
     && curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_to_str) == CURLE_OK
     && curl_easy_setopt(curl, CURLOPT_WRITEDATA, result) == CURLE_OK
     && curl_easy_perform(curl) == CURLE_OK) {
        ok = 1;
    }
    free(url);
    curl_slist_free_all(headers);
    curl_easy_cleanup(curl);
    if (ok) {
        return 1;
    }
    free(result->ptr);
    return 0;
}

static int parse_reply(const struct PtrSize* reply, struct Video** results, unsigned int* nbResults) {
    json_t *root, *e, *v;
    size_t i, n;
    int ok = 0;

    if (!(root = json_loadb(reply->ptr, reply->size, 0, NULL))) {
        return 0;
    }
    *results = NULL;
    if ((e = json_array_get(root, 1))
     && (e = json_object_get(e, "response"))
     && (e = json_object_get(e, "contents"))
     && (e = json_object_get(e, "twoColumnSearchResultsRenderer"))
     && (e = json_object_get(e, "primaryContents"))
     && (e = json_object_get(e, "sectionListRenderer"))
     && (e = json_object_get(e, "contents"))
     && (e = json_array_get(e, 0))
     && (e = json_object_get(e, "itemSectionRenderer"))
     && (e = json_object_get(e, "contents"))
     && json_is_array(e)) {
        n = json_array_size(e);
        *nbResults = 0;
        if ((*results = malloc(n * sizeof(struct Video)))) {
            for (i = 0; i < n; i++) {
                if ((v = json_array_get(e, i)) && (v = json_object_get(v, "videoRenderer"))) {
                    parse_video(v, (*results) + (*nbResults));
                    (*nbResults)++;
                }
            }
            ok = (i == n);
        }
    }
    json_decref(root);
    if (ok) {
        return 1;
    }
    free(*results);
    return 0;
}

int youtube_search(const char* query, struct Video** results, unsigned int* nbResults) {
    struct PtrSize reply = {NULL, 0};
    int ret = search(query, &reply) && parse_reply(&reply, results, nbResults);
    free(reply.ptr);
    return ret;
}

static int _youtube_search(const char* query, struct Video** results, unsigned int* nbResults, const void* pluginArgs) {
    return youtube_search(query, results, nbResults);
}

static const struct VideoPlugin _youtube = {
    _youtube_search,
    _youtube_get_video,
    _youtube_get_related_videos
};
const struct VideoPlugin* youtube = &_youtube;
