#include <netb1/webcomic.h>

#ifndef NETB1_WEBCOMIC_XKCD_H
#define NETB1_WEBCOMIC_XKCD_H

int xkcd_get_number(unsigned int number, struct Webcomic* dest);
int xkcd_get_latest(struct Webcomic* dest);

extern const struct WebcomicPlugin* xkcd;

#endif
