#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <jansson.h>
#include <netb1/browser/download.h>

#include "xkcd.h"

static char* json_copy_string(json_t* e, const char* name) {
    const char* s;
    char* ret = NULL;
    if ((e = json_object_get(e, name)) && (s = json_string_value(e)) && (ret = malloc(strlen(s) + 1))) {
        strcpy(ret, s);
    }
    return ret;
}

static char* xkcd_make_url(unsigned int num) {
    char* ret;
    if ((ret = malloc(64))) {
        sprintf(ret, "https://xkcd.com/%u", num);
    }
    return ret;
}

static int xkcd_get(const char* url, struct Webcomic* dest) {
    struct PtrSize result = {NULL, 0};
    CURL* curl;
    json_t *root, *e;
    unsigned int num;
    int ok;

    if (!(curl = curl_easy_init())) {
        return 0;
    }
    ok = set_user_agent(curl)
      && curl_easy_setopt(curl, CURLOPT_URL, url) == CURLE_OK
      && curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_to_str) == CURLE_OK
      && curl_easy_setopt(curl, CURLOPT_WRITEDATA, &result) == CURLE_OK
      && curl_easy_perform(curl) == CURLE_OK;
    curl_easy_cleanup(curl);

    if (!ok || !(root = json_loadb(result.ptr, result.size, 0, NULL))) {
        free(result.ptr);
        return 0;
    }
    ok = 0;
    if ((e = json_object_get(root, "num")) && (num = json_integer_value(e))) {
        dest->title = json_copy_string(root, "title");
        dest->description = json_copy_string(root, "alt");
        dest->imgUrl = json_copy_string(root, "img");
        dest->url = xkcd_make_url(num);
        dest->prevUrl = (num > 1) ? xkcd_make_url(num - 1) : NULL;
        dest->nextUrl = xkcd_make_url(num + 1) /* FIXME: check if it is available */;
        ok = 1;
    }

    json_decref(root);
    free(result.ptr);
    return ok;
}

int xkcd_get_number(unsigned int number, struct Webcomic* dest) {
    char url[64];
    sprintf(url, "https://xkcd.com/%u/info.0.json", number);
    return xkcd_get(url, dest);
}

int xkcd_get_latest(struct Webcomic* dest) {
    return xkcd_get("https://xkcd.com/info.0.json", dest);
}

static int xkcd_get_(const char* id, struct Webcomic* result, const void* pluginArgs) {
    if (!strncmp(id, "http", 4)) {
        id += 4;
        if (*id == 's') id++;
        if (!strncmp(id, "://", 3)) {
            id += 3;
        } else {
            return 0;
        }
    }
    if (!strncmp(id, "www.", 4)) id += 4;
    if (!strncmp(id, "xkcd.com", 8)) {
        id += 8;
        if (*id == '/') id++;
        if (!*id) {
            return xkcd_get_latest(result);
        }
    }
    return xkcd_get_number(strtoul(id, NULL, 10), result);
}

static int xkcd_get_first_(struct Webcomic* result, const void* pluginArgs) {
    return xkcd_get_number(1, result);
}

static int xkcd_get_last_(struct Webcomic* result, const void* pluginArgs) {
    return xkcd_get_latest(result);
}

static const struct WebcomicPlugin _xkcd = {
    xkcd_get_,
    xkcd_get_first_,
    xkcd_get_last_,
};
const struct WebcomicPlugin* xkcd = &_xkcd;
