#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <jansson.h>
#include <netb1/browser/download.h>
#include "wikipedia.h"

static char* json_copy_string(json_t* e, const char* name) {
    const char* s;
    char* ret = NULL;
    if ((e = json_object_get(e, name)) && (s = json_string_value(e)) && (ret = malloc(strlen(s) + 1))) {
        strcpy(ret, s);
    }
    return ret;
}

static char* get_url(json_t* e, const char* languageCode) {
    static const char articleUrl[] = "https://%s.wikipedia.org/?curid=%u";
    char* ret;
    if ((ret = malloc(sizeof(articleUrl) + strlen(languageCode) + 32))) {
        sprintf(ret, articleUrl, languageCode, (unsigned)json_integer_value(json_object_get(e, "pageid")));
    }
    return ret;
}

static unsigned long parse_date(const char* string) {
    unsigned int Y, M, D, h, m;
    float s;
    int tzh = 0, tzm = 0;
    if (sscanf(string, "%u-%u-%uT%u:%u:%f%d:%dZ", &Y, &M, &D, &h, &m, &s, &tzh, &tzm) >= 6) {
        struct tm tm;
        tm.tm_year = Y - 1900;
        tm.tm_mon = M - 1;
        tm.tm_mday = D;
        tm.tm_hour = h;
        tm.tm_min = m;
        tm.tm_sec = s;
        tm.tm_isdst = 0;
        if (tzh < 0) {
            tzm = -tzm;
        }
        return mktime(&tm) + (tzh * 60 + tzm) * 60;
    }
    return 0;
}

int wikipedia_search(const char* query, const char* languageCode, struct Article** articles, unsigned int* nbArticles) {
    static const char urlfmt[] = "https://%s.wikipedia.org/w/api.php?action=query&list=search&srprop=timestamp|snippet&format=json&utf8=&srsearch=%s";
    struct PtrSize result = {NULL, 0};
    char *encodedQuery = NULL, *url = NULL;
    CURL* curl;
    json_t *root, *e, *a;
    unsigned int i;
    int ok;

    if (!(curl = curl_easy_init())) {
        return 0;
    }
    ok = set_user_agent(curl)
      && (encodedQuery = curl_easy_escape(curl, query, 0))
      && (url = malloc(sizeof(urlfmt) + strlen(encodedQuery) + strlen(languageCode)))
      && sprintf(url, urlfmt, languageCode, encodedQuery) >= 0
      && curl_easy_setopt(curl, CURLOPT_URL, url) == CURLE_OK
      && curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_to_str) == CURLE_OK
      && curl_easy_setopt(curl, CURLOPT_WRITEDATA, &result) == CURLE_OK
      && curl_easy_perform(curl) == CURLE_OK;
    curl_free(encodedQuery);
    free(url);
    curl_easy_cleanup(curl);

    if (!ok || !(root = json_loadb(result.ptr, result.size, 0, NULL))) {
        free(result.ptr);
        return 0;
    }
    ok = 0;
    if ((e = json_object_get(root, "query")) && (e = json_object_get(e, "search"))) {
        if ((*articles = malloc((*nbArticles = json_array_size(e)) * sizeof(struct Article)))) {
            for (i = 0; i < *nbArticles; i++) {
                if ((a = json_array_get(e, i))) {
                    (*articles)[i].url = get_url(a, languageCode);
                    (*articles)[i].title = json_copy_string(a, "title");
                    (*articles)[i].author = NULL;
                    (*articles)[i].snippet = json_copy_string(a, "snippet");
                    (*articles)[i].content = NULL;
                    (*articles)[i].date = parse_date(json_string_value(json_object_get(a, "timestamp")));
                } else {
                    (*articles)[i].url = (*articles)[i].title = (*articles)[i].author = (*articles)[i].snippet = (*articles)[i].content = NULL;
                    (*articles)[i].date = 0;
                }
            }
            ok = 1;
        }
    }

    json_decref(root);
    free(result.ptr);
    return ok;
}

static int _wikipedia_search(const char* query, struct Article** articles, unsigned int* nbArticles, const void* pluginArgs) {
    return wikipedia_search(query, pluginArgs, articles, nbArticles);
}

int wikipedia_get_article(const char* string, const char* languageCode, struct Article* article) {
    static const char urlfmtTitle[] = "https://%s.wikipedia.org/w/api.php?format=json&action=parse&prop=wikitext&page=%s";
    static const char urlfmtId[] = "https://%s.wikipedia.org/w/api.php?format=json&action=parse&prop=wikitext&pageid=%s";
    struct PtrSize result = {NULL, 0};
    char *param = NULL, *url = NULL;
    const char *tmp, *end, *urlfmt;
    CURL* curl;
    json_t *root, *e;
    int ok;

    if (!(curl = curl_easy_init())) {
        return 0;
    }

    if ((tmp = strstr(string, "curid="))) {
        size_t size;
        for (end = tmp += 6; *end && *end != '&'; end++);
        if ((param = malloc((size = (end - tmp)) + 1))) {
            memcpy(param, tmp, size);
            param[size] = 0;
        }
        urlfmt = urlfmtId;
    } else if ((tmp = strstr(string, "/wiki/"))) {
        param = curl_easy_escape(curl, tmp + 6, 0);
        urlfmt = urlfmtTitle;
    } else {
        param = curl_easy_escape(curl, string, 0);
        urlfmt = urlfmtTitle;
    }

    ok = set_user_agent(curl)
      && (param)
      && (url = malloc(sizeof(urlfmtId) + strlen(param) + strlen(languageCode)))
      && sprintf(url, urlfmt, languageCode, param) >= 0
      && curl_easy_setopt(curl, CURLOPT_URL, url) == CURLE_OK
      && curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_to_str) == CURLE_OK
      && curl_easy_setopt(curl, CURLOPT_WRITEDATA, &result) == CURLE_OK
      && curl_easy_perform(curl) == CURLE_OK;
    if (urlfmt == urlfmtId) {
        free(param);
    } else {
        curl_free(param);
    }
    free(url);
    curl_easy_cleanup(curl);

    if (!ok || !(root = json_loadb(result.ptr, result.size, 0, NULL))) {
        free(result.ptr);
        return 0;
    }
    ok = 0;
    if ((e = json_object_get(root, "parse"))) {
        article->url = get_url(e, languageCode);
        article->title = json_copy_string(e, "title");
        article->author = NULL;
        article->snippet = NULL;
        article->content = json_copy_string(json_object_get(e, "wikitext"), "*");
        article->date = 0;
        ok = 1;
    }

    json_decref(root);
    free(result.ptr);
    return ok;
}

static int _wikipedia_get_article(const char* string, struct Article* article, const void* pluginArgs) {
    return wikipedia_get_article(string, pluginArgs, article);
}

static const struct ArticlePlugin _wikipedia = {
    _wikipedia_search,
    _wikipedia_get_article
};
const struct ArticlePlugin* wikipedia = &_wikipedia;
