#include <netb1/article.h>

#ifndef NETB1_ARTICLE_WIKIPEDIA_H
#define NETB1_ARTICLE_WIKIPEDIA_H

int wikipedia_search(const char* query, const char* languageCode, struct Article** articles, unsigned int* nbArticles);
int wikipedia_get_article(const char* string /* title or url */, const char* languageCode, struct Article* article);

extern const struct ArticlePlugin* wikipedia;

#endif
