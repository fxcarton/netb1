#include <stdlib.h>
#include <string.h>
#include <zlib.h>
#include <jansson.h>
#include <netb1/browser/download.h>
#include "stackoverflow.h"

static char* strdup(const char* orig) {
    char* ret;
    if (!orig) {
        return NULL;
    }
    if ((ret = malloc(strlen(orig) + 1))) {
        strcpy(ret, orig);
    }
    return ret;
}

int stackoverflow_search(const char* query, struct Thread** threads, unsigned int* nbThreads) {
    static const char urlBase[] = "https://api.stackexchange.com/2.2/search?site=stackoverflow&order=desc&sort=relevance&intitle=";
    struct PtrSize result = {NULL, 0};
    struct PtrSize decompressed = {NULL, 0};
    char *encodedQuery = NULL, *url = NULL, *tmp;
    CURL* curl;
    z_stream zstrm;
    json_t *root, *e, *t;
    unsigned int i;
    int ok, zres;

    if (!(curl = curl_easy_init())) {
        return 0;
    }
    ok = set_user_agent(curl)
      && (encodedQuery = curl_easy_escape(curl, query, 0))
      && (url = malloc(sizeof(urlBase) + strlen(encodedQuery)))
      && sprintf(url, "%s%s", urlBase, encodedQuery) >= 0
      && curl_easy_setopt(curl, CURLOPT_URL, url) == CURLE_OK
      && curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_to_str) == CURLE_OK
      && curl_easy_setopt(curl, CURLOPT_WRITEDATA, &result) == CURLE_OK
      && curl_easy_perform(curl) == CURLE_OK;
    curl_free(encodedQuery);
    free(url);
    curl_easy_cleanup(curl);

    zstrm.zalloc = NULL;
    zstrm.zfree = NULL;
    zstrm.opaque = NULL;
    zstrm.avail_in = result.size;
    zstrm.next_in = (unsigned char*)result.ptr;
    zstrm.avail_out = 0;
    if (!ok || inflateInit2(&zstrm, 15 + 32)) {
        free(result.ptr);
        return 0;
    }
    do {
        if (zstrm.avail_out == 0) {
            if (!(tmp = realloc(decompressed.ptr, decompressed.size + 1024))) {
                break;
            } else {
                decompressed.ptr = tmp;
                zstrm.next_out = (unsigned char*)decompressed.ptr + decompressed.size;
                zstrm.avail_out = 1024;
            }
        }
        zres = inflate(&zstrm, Z_NO_FLUSH);
        if (zres != Z_OK && zres != Z_STREAM_END) {
            break;
        }
        decompressed.size += 1024 - zstrm.avail_out;
    } while (zres != Z_STREAM_END);
    inflateEnd(&zstrm);
    free(result.ptr);
    ok = (zres == Z_STREAM_END);

    if (!ok || !(root = json_loadb(decompressed.ptr, decompressed.size, 0, NULL))) {
        free(decompressed.ptr);
        return 0;
    }
    ok = 0;
    if ((e = json_object_get(root, "items"))) {
        *nbThreads = json_array_size(e);
        if ((*threads = malloc(*nbThreads * sizeof(struct Thread)))) {
            for (i = 0; i < *nbThreads; i++) {
                t = json_array_get(e, i);
                (*threads)[i].url = strdup(json_string_value(json_object_get(t, "link")));
                (*threads)[i].title = strdup(json_string_value(json_object_get(t, "title")));
                (*threads)[i].author = strdup(json_string_value(json_object_get(json_object_get(t, "owner"), "display_name")));
                (*threads)[i].creationDate = json_integer_value(json_object_get(t, "creation_date"));
                (*threads)[i].lastModificationDate = json_integer_value(json_object_get(t, "last_activity_date"));
            }
            ok = 1;
        }
    }
    json_decref(root);
    free(decompressed.ptr);
    return ok;
}

static int _stackoverflow_search(const char* query, struct Thread** threads, unsigned int* nbThreads, const void* pluginArgs) {
    return stackoverflow_search(query, threads, nbThreads);
}

static const struct ForumPlugin _stackoverflow = {
    _stackoverflow_search
};
const struct ForumPlugin* stackoverflow = &_stackoverflow;
