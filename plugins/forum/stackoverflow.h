#include <netb1/forum.h>

#ifndef NETB1_FORUM_STACKOVERFLOW_H
#define NETB1_FORUM_STACKOVERFLOW_H

int stackoverflow_search(const char* query, struct Thread** threads, unsigned int* nbThreads);

extern const struct ForumPlugin* stackoverflow;

#endif
