#!/bin/bash

VIDEO_PLAYER="mpv"
VIDEO_OPTIONS=$(test -n "$AUDIO_DEVICE" && echo "--audio-device=alsa/dmix:$AUDIO_DEVICE")
BROWSER="surf"

yt() {
    URL=$(youtube search "$1" | choice)
    while true ; do
        ID="${URL#"https://www.youtube.com/watch?v="}"
        youtube info "$ID"
        $VIDEO_PLAYER $VIDEO_OPTIONS "$URL"
        URL="$(youtube info --related "$ID" | sed 1d | choice -t 10)"
    done

}

yt-novid() {
    VIDEO_OPTIONS="--vid=no $VIDEO_OPTIONS"
    yt "$1"
}

sp() {
    URL=$(startpage search "$1" | choice)
    $BROWSER "$URL"
}

wp() {
    URL=$(wikipedia search "$1" | choice)
    $BROWSER "$URL"
}

$1 "$2"
