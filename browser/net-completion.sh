net_get_providers() {
    printf '@world\n'
    grep '^WORLD=' "$(command -v net)" | grep -o '\$[A-Z]*' | tr '$[:upper:]' '@[:lower:]'
    grep -E "^($(grep '^WORLD=' "$(command -v net)" | cut -d'"' -f2 | tr -d '$' | tr ' ' '|'))=" "$(command -v net)" | cut -d'"' -f2 | sed 's/\([a-z]*\) */@\1\n/' | grep -v '^$'
}

net_completion() {
    if [ "$COMP_CWORD" -eq 1 ]; then
        COMPREPLY=( $(compgen -W "search https:// http://" -- "${COMP_WORDS[COMP_CWORD]}") )
    elif [ "${COMP_WORDS[1]}" = "search" ]; then
        if [ "${COMP_WORDS[COMP_CWORD-1]}" = "@" ]; then
            cur="@${COMP_WORDS[COMP_CWORD]}"
        else
            cur="${COMP_WORDS[COMP_CWORD]}"
        fi
        COMPREPLY=( $(compgen -W "$(net_get_providers)" -- "$cur") )
    fi
}
complete -F net_completion net
