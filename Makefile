CFLAGS ?= -g -Wall -std=c99 -pedantic
PREFIX ?= /usr

###

LIB := libnetb1.so
LIB_DEPS := libcurl jansson zlib libxml-2.0
LIB_SOURCES := $(wildcard netb1/*.c netb1/browser/*.c)
LIB_OBJECTS := $(LIB_SOURCES:.c=.o)
PLUGINS := $(patsubst %.c,%.so,$(wildcard plugins/**/*.c))
PROGS := $(patsubst %.c,%,$(wildcard progs/**/*.c))

CFLAGS += -I. $(shell pkg-config --cflags $(LIB_DEPS))
LDFLAGS += $(shell pkg-config --libs-only-L --libs-only-other $(LIB_DEPS))
LDLIBS += $(shell pkg-config --libs-only-l $(LIB_DEPS))

.PHONY: all
all: $(LIB) $(PLUGINS) $(PROGS)

.PHONY: clean
clean:
	rm -f $(wildcard $(LIB_OBJECTS) $(LIB) $(PLUGINS) $(PROGS:%=%.o) $(PROGS))

.PHONY: install
install:
	mkdir -p $(DESTDIR)/$(PREFIX)/bin $(DESTDIR)/$(PREFIX)/lib $(DESTDIR)/$(PREFIX)/share/netb1/plugins
	cp $(PROGS) $(DESTDIR)/$(PREFIX)/bin
	cp $(LIB) $(DESTDIR)/$(PREFIX)/lib
	cp $(PLUGINS) $(DESTDIR)/$(PREFIX)/share/netb1/plugins

tags: $(LIB_SOURCES)
	ctags $^

$(LIB_OBJECTS): CFLAGS += -fPIC
$(LIB): $(LIB_OBJECTS)
	$(CC) -shared -fPIC $(LDFLAGS) -o $@ $^ $(LDLIBS)

$(PLUGINS): %.so: %.c
	$(CC) -shared -fPIC $(CFLAGS) $(LDFLAGS) -o $@ $^ $(LDLIBS)

$(PROGS): %: %.o $(LIB)
	$(CC) $(LDFLAGS) -o $@ $(realpath $^) $(LDLIBS)
progs/video/youtube: plugins/video/youtube.o
progs/searchengine/startpage: plugins/searchengine/startpage.o
progs/forum/stackoverflow: plugins/forum/stackoverflow.o
progs/webcomic/xkcd: plugins/webcomic/xkcd.o
progs/article/wikipedia: plugins/article/wikipedia.o
